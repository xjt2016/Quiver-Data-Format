package com.gitee.xjt2016;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.gitee.xjt2016.domain.PrintBgTrade;
import com.gitee.xjt2016.domain.PrintHandOrders;
import com.gitee.xjt2016.domain.ReceiverAddress;
import com.gitee.xjt2016.modules.common.utils.ListUtils;
import com.google.common.collect.Lists;
import io.gitee.xjt2016.modules.common.utils.RandomValue;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.junit.Assert;
import org.junit.Test;
import org.nutz.dao.Dao;
import org.nutz.dao.entity.Record;
import org.nutz.dao.impl.NutDao;
import org.nutz.dao.impl.SimpleDataSource;
import org.nutz.dao.util.Daos;

import java.util.*;

/**
 * 造数据.
 *
 * @author xiongjinteng@raycloud.com
 */
@Log4j2
public class CreateBigData {

    public static SimpleDataSource dataSource;

    public static final SnowflakeIdWorker idWorker = new SnowflakeIdWorker(0, 0);

    static {
        // 创建一个数据源
        dataSource = new SimpleDataSource();
        try {
            dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        dataSource.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/kdzs_xcx?useSSL=false&amp;useUnicode=true&amp;characterEncoding=utf-8&amp;autoReconnect=true&amp;allowMultiQueries=true");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");

    }

    @Test
    public void routeUser() {
        Dao dao = new NutDao(dataSource);

        Daos.FORCE_WRAP_COLUMN_NAME = true;

        List<Record> recordList = dao.query("route_user_2", null);
        Assert.assertEquals(48691, recordList.size());
        Set<Long> taobaoIds = new HashSet<>();
        for (Record record : recordList) {
            Long taobaoId = record.getLong("taobao_id");
            taobaoIds.add(taobaoId);
        }
        List<List<Long>> subLists = ListUtils.split2List(new ArrayList<>(taobaoIds), 1000);
        //System.out.println(subLists.size());
        for (List<Long> subList : subLists) {
            String sql = "update route_user set url_domain='p50.kuaidizs.com' where taobao_id in (" + StringUtils.join(subList, ",") + ");\n";
            System.out.println(sql);
        }

    }

    @Test
    public void Create() {
        // 创建一个NutDao实例,在真实项目中, NutDao通常由ioc托管, 使用注入的方式获得.
        Dao dao = new NutDao(dataSource);

        Daos.FORCE_WRAP_COLUMN_NAME = true;

        // 创建表
        //dao.create(PrintBgTrade.class, false); // false的含义是,如果表已经存在,就不要删除重建了.

        //int count = dao.count(PrintBgTrade.class);
        //System.out.println(count);


        List<PrintBgTrade> list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            PrintBgTrade printBgTrade = new PrintBgTrade();
            printBgTrade.setTradeId(idWorker.nextId());
            printBgTrade.setUserId(RandomUtil.randomLong(1, 50));

            boolean isTb = RandomUtils.nextBoolean();

            if (isTb) {
                Long randomValue = Long.valueOf(60122949 + "" + RandomUtil.randomLong(1, 100));
                printBgTrade.setTaobaoId(randomValue);
                printBgTrade.setCainiaoTokenId(null);
            } else {
                printBgTrade.setTaobaoId(null);

                Long randomValue = Long.valueOf(60122949 + "" + RandomUtil.randomLong(1, 100));
                printBgTrade.setCainiaoTokenId(randomValue);
            }

            Date date = RandomUtil.randomDay(-90, 1);
            printBgTrade.setCreatedAt(date);
            printBgTrade.setUpdatedAt(date);
            printBgTrade.setEnable(1);
            printBgTrade.setStatus(RandomUtil.randomEle(Lists.newArrayList("noPrint", "printSuccess")));

            list.add(printBgTrade);
            if (list.size() > 20000) {
                dao.fastInsert(list);
                list.clear();
            }
            //dao.insert(printBgTrade);
        }
        if (CollectionUtil.isNotEmpty(list)) {
            dao.fastInsert(list);
        }
    }

    @Test
    public void createReceiverAddress() {
        // 创建一个NutDao实例,在真实项目中, NutDao通常由ioc托管, 使用注入的方式获得.
        Dao dao = new NutDao(dataSource);

        Daos.FORCE_WRAP_COLUMN_NAME = true;

        // 创建表
        //dao.create(PrintBgTrade.class, false); // false的含义是,如果表已经存在,就不要删除重建了.
        //dao.clear(ReceiverAddress.class);
        //int count = dao.count(ReceiverAddress.class);

        List<ReceiverAddress> list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            ReceiverAddress receiverAddress = new ReceiverAddress();
            receiverAddress.setId(idWorker.nextId());
            receiverAddress.setUserId(RandomUtil.randomLong(1, 50));
            receiverAddress.setName(RandomValue.getChineseName());
            receiverAddress.setPhone(RandomValue.getMobile());

            Date date = RandomUtil.randomDay(-90, 0);
            receiverAddress.setCreatedAt(date);
            receiverAddress.setUpdatedAt(date);
            receiverAddress.setEnable(1);

            list.add(receiverAddress);
            if (list.size() > 200000) {
                dao.fastInsert(list);
                log.debug("批量插入数据" + DateUtil.formatDateTime(new Date()));
                list.clear();
            }
        }

        if (CollectionUtil.isNotEmpty(list)) {
            dao.fastInsert(list);
        }
    }


    @Test
    public void json() {
        // 创建一个NutDao实例,在真实项目中, NutDao通常由ioc托管, 使用注入的方式获得.
        Dao dao = new NutDao(dataSource);

        Daos.FORCE_WRAP_COLUMN_NAME = true;

        // 创建表
        //dao.create(PrintBgTrade.class, false); // false的含义是,如果表已经存在,就不要删除重建了.
        //dao.clear(ReceiverAddress.class);
        //int count = dao.count(PrintHandOrders.class);
        //System.out.println(count);
        dao.truncate(PrintHandOrders.class);
        for (int j = 0; j < 5; j++) {
            Long userId = RandomUtil.randomLong(1, 50);
            List<PrintHandOrders> list = new ArrayList<>();
            for (int i = 0; i < 200000; i++) {
                PrintHandOrders handOrders = new PrintHandOrders();
                handOrders.setUserId(userId);
                handOrders.setCreated(RandomUtil.randomDay(-90, 0));
                list.add(handOrders);
            }
            dao.fastInsert(list);
            list.clear();
        }
    }
}
