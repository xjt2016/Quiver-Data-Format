package com.gitee.xjt2016.domain;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Table;

import java.util.Date;

@Table("route_user_2")
@Getter
@Setter
public class RouteUser {

    /**
     * 主键id
     */
    private Long id;
    /**
     * 创建时间
     */
    private Date created;
    /**
     * 修改时间
     */
    private Date modified;
    /**
     * 逻辑删除状态0表示删除 1表示未删除
     */
    private Boolean enableStatus;
    /**
     * 淘宝id
     */
    private Long taobaoId;
    /**
     * 域名URL
     */
    private String urlDomain;
}
