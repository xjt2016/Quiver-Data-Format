package com.gitee.xjt2016.domain;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

import java.io.Serializable;
import java.util.Date;

/**
 * ReceiverAddress.
 *
 * @author xiongjinteng@raycloud.com
 */
@Table("receiver_address")
@Getter
@Setter
public class ReceiverAddress implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @Column(hump = true)
    private Long id;
    /**
     * user_id
     */
    @Column(hump = true)
    private Long userId;

    /**
     * name
     */
    @Column(hump = true)
    private String name;
    /**
     * company
     */
    @Column(hump = true)
    private String company;
    /**
     * state
     */
    @Column(hump = true)
    private String state;
    /**
     * city
     */
    @Column(hump = true)
    private String city;
    /**
     * district
     */
    @Column(hump = true)
    private String district;
    /**
     * address
     */
    @Column(hump = true)
    private String address;
    /**
     * email
     */
    @Column(hump = true)
    private String email;
    /**
     * mobile
     */
    @Column(hump = true)
    private String mobile;
    /**
     * phone
     */
    @Column(hump = true)
    private String phone;
    /**
     * zip
     */
    @Column(hump = true)
    private String zip;
    /**
     * enable
     */
    @Column(hump = true)
    private Integer enable;
    /**
     * created_at
     */
    @Column(hump = true)
    private Date createdAt;
    /**
     * updated_at
     */
    @Column(hump = true)
    private Date updatedAt;
}