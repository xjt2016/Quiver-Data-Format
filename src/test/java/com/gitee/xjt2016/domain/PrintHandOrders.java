package com.gitee.xjt2016.domain;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * PrintHandOrders.
 *
 * @author xiongjinteng@raycloud.com
 */
@Table(value = "print_hand_orders_tb")
@Getter
@Setter
public class PrintHandOrders implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 主键自增id
     */
    @Id
    @Column(hump = true)
    private Long id;
    /**
     * 用户id
     */
    @Column(hump = true)
    private Long userId;
    /**
     * tid流水号
     */
    @Column(hump = true)
    private String tid;

    /**
     * 隐藏id
     */
    @Column(hump = true)
    private String realTid;

    /**
     * 创建日期
     */
    @Column(hump = true)
    private Date created;
    /**
     * 收件人
     */
    @Column(hump = true)
    private String recipient;
    /**
     * 固话
     */
    @Column(hump = true)
    private String receiverTel;
    /**
     * 手机
     */
    @Column(hump = true)
    private String receiverPhone;
    /**
     * 快递单号信息
     */
    @Column(hump = true)
    private String exnumber;
    /**
     * 省
     */
    @Column(hump = true)
    private String receiverProvince;
    /**
     * 市
     */
    @Column(hump = true)
    private String receiverCity;
    /**
     * 区
     */
    @Column(hump = true)
    private String receiverCounty;
    /**
     * 收件地址
     */
    @Column(hump = true)
    private String receiverAddress;
    /**
     * 发件信息
     */
    @Column(hump = true)
    private String sendinfo;
    /**
     * 是否打印(0)/打印次数
     */
    @Column(hump = true)
    private Integer isPrint;
    /**
     * 是否删除
     */
    private Boolean isDel;
    /**
     * 发件人   //以下套打新加字段
     */
    @Column(hump = true)
    private String senderName;
    /**
     * 发件人电话
     */
    @Column(hump = true)
    private String senderTel;

    /**
     * 发件人手机
     */
    @Column(hump = true)
    private String senderPhone;
    /**
     * 发件人地址
     */
    @Column(hump = true)
    private String senderAddress;
    /**
     * 加单编号
     */
    private String addExNumber;
    /**
     * 加单件数
     */
    private String addExCount;
    /**
     * 加单业务员
     */
    private String addExName;
    /**
     * 内件名
     */
    private String internalName;
    /**
     * 0手打 1套打
     */
    private Integer type;
    /**
     * 快递单号
     */
    private String trueExNumber;
    /**
     * 是否可修改发件人信息，0否1是
     */
    private Integer isModifySender = 0;
    /**
     * 是否发货 0未发货 1已发货
     */
    private Integer isFh;
    /**
     * 是否为云栈电子面单打印，0不是 1是
     */
    private Integer isYunzhan;
    /**
     * 订单号是否回收（0没回收 1已回收）
     */
    private Integer exIsHuishou;
    /**
     * 是否为淘宝发货订单，0不是 1是
     */
    private Integer isTbSend;
    /**
     * 备注
     */
    private String remark;
    /**
     * 代收金额
     */
    private BigDecimal collectionMoney;
    /**
     * 代收货款字符串
     */
    private String collectionMoneyStr;
    /**
     * 保价金额
     */
    private BigDecimal declarationValue;
    /**
     * 保价金额字符串
     */
    private String declarationValueStr;
    /**
     * 业务类型
     */
    private String businessType;
    /**
     * 回收来源 默认0：正常订单 1：已打印订单编辑  2：重打非当前模板
     */
    private Integer recyclingSources;
    /**
     * modified
     */
    private Date modified;
    /**
     * enable_status
     */
    private Boolean enableStatus;

    /**
     * 发件人信息ID 对应ModeSetFjrKdd中的ID
     */
    private Long senderId;

    /**
     * 数量
     */
    private Integer num;
    /**
     * 数量字符串
     */
    private String numStr;
    /**
     * 优惠
     */
    private String discount;
    /**
     * 实付
     */
    private String payment;
    /**
     * weight
     */
    private Integer weight;

    private String weightStr;

    /**
     * 运费
     */
    private String postFee;
    /**
     * 备注旗帜
     */
    private String remarkFlag;
    /**
     * 是否打印发货单
     */
    private String isFhd;


    /**
     * 发货状态 0是未发货 1是已发货
     */
    private Integer sendStatus;
    /**
     * 发货时间
     */
    private Date sendTime;

    /**
     * 旺旺
     */
    private String ww;
    /**
     * 拓展字段
     */
    private String propertites;

}