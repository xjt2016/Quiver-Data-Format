package com.gitee.xjt2016.domain;

import lombok.Getter;
import lombok.Setter;
import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

import java.io.Serializable;
import java.util.Date;

@Table("print_bg_trade")
@Getter
@Setter
public class PrintBgTrade implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(hump = true)
    private Long id;

    @Column(hump = true)
    private Long tradeId;

    @Column(hump = true)
    private Long userId;

    @Column(hump = true)
    private Long taobaoId;

    @Column(hump = true)
    private Long cainiaoTokenId;

    @Column(hump = true)
    private String status;

    @Column(hump = true)
    private Integer enable;

    @Column(hump = true)
    private Date createdAt;

    @Column(hump = true)
    private Date updatedAt;
}
