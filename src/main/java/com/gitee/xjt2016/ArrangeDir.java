package com.gitee.xjt2016;

import cn.hutool.core.io.FileUtil;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

/**
 * <p>依赖上面的包，然后运行如下程序即可整理某个目录的文档</p>
 *
 * @author xiongjinteng@raycloud.com
 */
public class ArrangeDir {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String path = "/Users/xjt2016/Downloads/";//修改为你要整理的目录
        FileUtil.loopFiles(path);
        List<String> fileNameList = FileUtil.listFileNames(path);
        System.out.println("总文件个数：" + fileNameList.size());
        for (String fileName : fileNameList) {
            File file = new File(path + "/" + fileName);
            if (file.isDirectory() || !fileName.contains(".")) {
                continue;
            }
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();

            String newFileName = URLDecoder.decode(file.getName(), "utf-8");
            String newPathName = path + "/" + suffix + "/" + newFileName;
            if (fileName.startsWith(".")) {
                System.err.println(fileName);
                continue;
                //FileUtil.del(path + fileName);
            } else if ("bmp,jpg,jpeg,png,tif,gif,pcx,tga,exif,fpx,svg,psd,cdr,pcd,dxf,ufo,eps,ai,raw,WMF,webp".contains(suffix)) {
                newPathName = path + "/" + "images" + "/" + newFileName;
            } else if ("xls,xlsx,xlsm,csv".contains(suffix)) {
                newPathName = path + "/" + "excel" + "/" + newFileName;
            } else if ("rtf,,txt,doc,docx,ppt,pages,xmind,sql,md,spf,js,css,xml,json,java,html".contains(suffix)) {
                newPathName = path + "/" + "excel" + "/" + newFileName;
            }
            FileUtil.move(file, new File(newPathName), false);
        }
        arrangeEpub();
    }

    private static void arrangeEpub() {
        String epubPath = "/Users/xjt2016/Downloads/epub/";
        List<String> fileNameList = FileUtil.listFileNames(epubPath);
        for (String fileName : fileNameList) {
            try {
                final String originFileName = fileName;
                fileName = fileName.replaceAll("（", "(").replaceAll("）", ")");
                String newFileName0 = fileName;
                try {
                    newFileName0 = URLDecoder.decode(StringUtils.trim(fileName), "utf-8");
                } catch (Exception e) {
                    System.out.println("文件名解码失败：" + fileName);
                }
                if (!StringUtils.equals(fileName, newFileName0)) {
                    fileName = newFileName0;
                }
                if (fileName.contains("_")) {
                    fileName = fileName.substring(fileName.lastIndexOf("_") + 1);
                }
                File file = new File(epubPath + originFileName);
                FileUtil.rename(file, fileName, false, true);
            } catch (Exception e) {
                System.out.println(fileName + "," + e.getMessage());
            }
        }
    }
}
