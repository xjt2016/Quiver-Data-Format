package com.gitee.xjt2016.modules.quiver;

import lombok.Data;

@Data
public class Quiver {

    private String name;

    private String uuid;
}
