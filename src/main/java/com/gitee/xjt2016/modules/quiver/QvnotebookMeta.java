package com.gitee.xjt2016.modules.quiver;

import lombok.Data;

import java.util.List;

@Data
public class QvnotebookMeta {

    private String uuid;
    private String name;
    private List<MetaChildren> children;

    @Override
    public String toString() {
        return "QvnotebookMeta{" +
                "uuid='" + uuid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
