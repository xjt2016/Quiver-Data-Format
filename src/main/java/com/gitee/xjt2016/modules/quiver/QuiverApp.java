package com.gitee.xjt2016.modules.quiver;

import org.nutz.json.Json;

import java.util.List;

/**
 * <p>Quiver：数据目录</p>
 * <p>qvnotebook：分类目录</p>
 * <p>qvnote：文章，包含：content.json,qvnotebookMeta.json,resource文件夹.</p>
 *
 * @author xiongjinteng@raycloud.com
 */
public class QuiverApp {
    private static final String path = "/Users/xjt2016/Downloads/Quiver.qvlibrary";
    private static Qvlibrary qvlibrary;

    static {
        qvlibrary = Qvlibrary.loadPath(path);
    }

    public static void main(String[] args) {
        //for (Qvnotebook qvnotebook : qvlibrary.getQvnotebookList()) {
        //    System.out.println(qvnotebook.getName());
        //}
        //for (MetaChildren metaChildren : qvlibrary.getQvnotebookMeta().getChildren()) {
        //    Qvlibrary.recursiveQvnotebook(metaChildren, qvlibrary.getQvnotebookMap());
        //}
        List<TreeNode> list = Qvlibrary.renderTreeNode(qvlibrary);
        System.out.println(Json.toJson(list));
    }
}
