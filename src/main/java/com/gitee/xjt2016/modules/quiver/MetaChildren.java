package com.gitee.xjt2016.modules.quiver;

import lombok.Data;

import java.util.List;

@Data
public class MetaChildren {
    private String uuid;
    private List<MetaChildren> children;
}
