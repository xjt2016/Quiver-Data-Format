package com.gitee.xjt2016.modules.quiver;

import lombok.Data;

import java.util.Date;

@Data
public class QvnoteMeta {

    private String uuid;

    private String title;

    private Date updated_at;

    private Date created_at;

    private String[] tags;
}
