package com.gitee.xjt2016.modules.quiver;


import lombok.Data;
import lombok.NonNull;
import org.apache.commons.collections.CollectionUtils;
import org.nutz.json.Json;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Qvlibrary extends Quiver {

    private QvnotebookMeta qvnotebookMeta;

    //private List<Qvnotebook> qvnotebookList;

    private Map<String, Qvnotebook> qvnotebookMap = new HashMap<>();

    private String path;

    private boolean isLoaded = false;

    public static Qvlibrary loadPath(String path) {
        if (path == null || path.length() <= 0) {
            throw new IllegalArgumentException("param is blank");
        }
        Qvlibrary qvlibrary = new Qvlibrary(path);
        qvlibrary.load();
        return qvlibrary;
    }

    public Qvlibrary(String path) {
        this.path = path;
        this.setQvnotebookMeta(Json.fromJsonFile(QvnotebookMeta.class, new File(path + "/meta.json")));
    }

    private void load() {
        File qvlibraryFile = new File(this.getPath());
        assert qvlibraryFile.isDirectory();
        File[] qvnotebookFiles = qvlibraryFile.listFiles();
        //List<Qvnotebook> qvnotebookList = new ArrayList<>();
        if (qvnotebookFiles != null && qvnotebookFiles.length > 0) {
            for (File qvnotebookFile : qvnotebookFiles) {
                if (qvnotebookFile.isDirectory()) {
                    Qvnotebook qvnotebook = new Qvnotebook(qvnotebookFile);
                    //qvnotebookList.add(qvnotebook);
                    this.getQvnotebookMap().put(qvnotebook.getUuid(), qvnotebook);
                }
            }
        }
        isLoaded = true;
        //this.setQvnotebookList(qvnotebookList);
    }

//    private static void renderMarkdown(String renderPath, Qvlibrary qvlibrary) {
//        FileUtil.clean(renderPath);
//        List<Qvnotebook> qvnotebookList = qvlibrary.getQvnotebookList();
//
//        String resourcesPath = renderPath + "/resources";
//        FileUtil.del(new File(resourcesPath));
//
//        if (CollectionUtil.isNotEmpty(qvnotebookList)) {
//            for (Qvnotebook qvnotebook : qvnotebookList) {
//                List<Qvnote> qvnoteList = qvnotebook.getQvnoteList();
//                if (CollectionUtil.isNotEmpty(qvnoteList)) {
//                    for (Qvnote qvnote : qvnoteList) {
//                        String qvnotePath = qvnote.getPath();
//
//                        File qvnoteResourcesFile = new File(qvnotePath + "/resources");
//                        if (qvnoteResourcesFile.exists()) {
//                            FileUtil.copy(qvnoteResourcesFile, new File(renderPath), true);
//                        }
//
//                        String fileName = qvnote.getQvnoteMeta().getTitle().replaceAll("\\/", " ");
//                        String suffix = ".md";
//                        File mdFile;
//                        int i = 0;
//
//                        do {
//                            if (i == 0) {
//                                mdFile = new File(renderPath + "/" + fileName + suffix);
//                            } else {
//                                mdFile = new File(renderPath + "/" + fileName + "(" + i + ")" + suffix);
//                            }
//                            i++;
//                        } while (mdFile.exists());
//
//                        try {
//                            boolean createNewFile = mdFile.createNewFile();
//                            Article article = qvnote.getArticle();
//                            if (article != null) {
//                                CollectionUtil.isNotEmpty(article.getCells());
//                                for (Cell cell : article.getCells()) {
//                                    String data = cell.getData();
//                                    data = data.replaceAll("quiver-image-url", "resources");
//                                    FileUtil.appendUtf8String(data, mdFile);
//                                    FileUtil.appendUtf8String("\n", mdFile);
//                                }
//                            }
//                        } catch (Exception e) {
//                            System.out.println(fileName);
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }
//        }
//    }

    public static List<TreeNode> renderTreeNode(@NonNull Qvlibrary qvlibrary) {
        QvnotebookMeta rootMeta = qvlibrary.getQvnotebookMeta();
        Map<String, Qvnotebook> qvnotebookMap = qvlibrary.getQvnotebookMap();
        assert rootMeta != null;
        List<TreeNode> list = new ArrayList<>();
        list.add(new TreeNode(rootMeta.getUuid(), "0", rootMeta.getUuid()));
        if (CollectionUtils.isNotEmpty(rootMeta.getChildren())) {
            for (MetaChildren metaChildren : rootMeta.getChildren()) {
                recursiveQvnotebook(list, rootMeta.getUuid(), metaChildren, qvnotebookMap);

            }
        }
        return list;
    }

    public static void recursiveQvnotebook(List<TreeNode> list, String pid, MetaChildren metaChildren, Map<String, Qvnotebook> qvnotebookMap) {
        Qvnotebook qvnotebook = qvnotebookMap.get(metaChildren.getUuid());
        //recursiveQvnote(qvnotebook);
        list.add(new TreeNode(qvnotebook.getUuid(), pid, qvnotebook.getName()));
        if (CollectionUtils.isNotEmpty(metaChildren.getChildren())) {
            for (MetaChildren child : metaChildren.getChildren()) {
                recursiveQvnotebook(list, qvnotebook.getUuid(), child, qvnotebookMap);
            }
        }
    }


    public static void recursiveQvnotebook(MetaChildren metaChildren, Map<String, Qvnotebook> qvnotebookMap) {
        Qvnotebook qvnotebook = qvnotebookMap.get(metaChildren.getUuid());
        System.out.println(qvnotebook.getName());
        recursiveQvnote(qvnotebook);
        if (CollectionUtils.isNotEmpty(metaChildren.getChildren())) {
            for (MetaChildren child : metaChildren.getChildren()) {
                recursiveQvnotebook(child, qvnotebookMap);
            }
        }
    }

    public static void recursiveQvnote(Qvnotebook qvnotebook) {
        if (CollectionUtils.isNotEmpty(qvnotebook.getQvnoteList())) {
            for (Qvnote qvnote : qvnotebook.getQvnoteList()) {
                System.out.println("  " + qvnote.getQvnoteMeta().getTitle());
            }
        }
    }
}
