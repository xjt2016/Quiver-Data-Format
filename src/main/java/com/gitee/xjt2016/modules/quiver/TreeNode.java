package com.gitee.xjt2016.modules.quiver;

import lombok.Data;

@Data
public class TreeNode {

    private String id;

    private String pId;

    private String name;

    public TreeNode() {
    }

    public TreeNode(String id, String pId, String name) {
        this.id = id;
        this.pId = pId;
        this.name = name;
    }
}
