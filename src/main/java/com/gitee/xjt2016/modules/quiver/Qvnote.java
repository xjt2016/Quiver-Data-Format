package com.gitee.xjt2016.modules.quiver;

import com.gitee.xjt2016.Article;
import lombok.Data;

/**
 * Qvnote.
 * <p>qvnote：文章，包含：content.json,qvnotebookMeta.json,resource文件夹.</p>
 *
 * @author xiongjinteng@raycloud.com
 */
@Data
public class Qvnote {

    private QvnoteMeta qvnoteMeta;

    private Article article;

    private String path;

    private String resourcesPath;

}
