package com.gitee.xjt2016;

import com.gitee.xjt2016.modules.quiver.Cell;
import lombok.Data;

import java.util.List;

@Data
public class Article {

    private String title;

    private List<Cell> cells;
}
